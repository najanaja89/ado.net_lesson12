﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ado.net_lesson12.Models;
using ado.net_lesson12.Repository;
using Dapper;

namespace ado.net_lesson12
{
    class Program
    {
        static void Main(string[] args)
        {
            StudentRepository studentRepository = new StudentRepository();
            do
            {
                Console.Clear();
                string menu = "";
                Console.WriteLine("Press 1 to add student");
                Console.WriteLine("Press 2 to edit student info");
                Console.WriteLine("Press 3 to delete student");
                Console.WriteLine("Press 4 to view list of students");
                Console.WriteLine("Press 5 to view group with most visit count");
                Console.WriteLine("Press 6 to view students by date of visit");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        Console.WriteLine("Enter FirstName");
                        string firstName = Console.ReadLine();
                        Console.WriteLine("Enter LastName");
                        string lastName = Console.ReadLine();
                        Console.WriteLine("Enter GroupId");
                        string groupId = Console.ReadLine();

                        if (firstName == "" || lastName == "")
                        {
                            Console.WriteLine("First Name or Last Name in empty");
                            break;
                        }

                        else if (Regex.IsMatch(groupId, @"^[0 - 9] +$") || groupId == "")
                        {
                            Console.WriteLine("group id must be digit");
                        }

                        else
                        {
                            studentRepository.Insert(@"INSERT INTO Students (FirstName,LastName, GroupId) VALUES (@FirstName,@LastName, @GroupId)", new Student()
                            {
                                FirstName = firstName,
                                LastName = lastName,
                                GroupId = int.Parse(groupId)
                            });
                        }
                        break;
                    case "2":
                        Console.WriteLine("Enter Id");
                        string id = Console.ReadLine();
                        Console.WriteLine("Enter FirstName");
                        firstName = Console.ReadLine();
                        Console.WriteLine("Enter LastName");
                        lastName = Console.ReadLine();
                        Console.WriteLine("Enter GroupId");
                        groupId = Console.ReadLine();

                        if (firstName == "" || lastName == "")
                        {
                            Console.WriteLine("First Name or Last Name in empty");
                            break;
                        }

                        else if (Regex.IsMatch(groupId, @"^[0 - 9] +$") || groupId == "")
                        {
                            Console.WriteLine("group id must be digit");
                        }

                        else if (Regex.IsMatch(id, @"^[0 - 9] +$") || id == "")
                        {
                            Console.WriteLine("id must be digit");
                        }

                        studentRepository.Update(@"update Students set FirstName=@FirstName, LastName=@LastName, GroupId=@GroupId where Id=@Id", new Student
                        {
                            Id = int.Parse(id),
                            FirstName = firstName,
                            LastName = lastName,
                            GroupId = int.Parse(groupId)

                        });
                        break;

                    case "3":
                        Console.WriteLine("Enter Id");
                        id = Console.ReadLine();
                        if (Regex.IsMatch(id, @"^[0 - 9] +$") || id == "")
                        {
                            Console.WriteLine("group id must be digit");
                        }

                        else
                        {
                            studentRepository.Delete(@"delete from Students where Id=@Id", new Student
                            {
                                Id = int.Parse(id)
                            });
                        }
                        break;

                    case "4":
                        var students = studentRepository.GetAllStudent(@"select Students.Id, Students.FirstName, Students.LastName, Students.GroupId, Groups.GroupName from Students 
                            join  Groups on Groups.Id=GroupId").ToList();
                        students.OrderBy(x => x.GroupName);
                        students.ForEach(f => { Console.WriteLine($"\t\tStudent Id: {f.Id}\t\t{f.FirstName}\t\t{f.LastName}\t\t\t{f.GroupName}\t\tGroup Id:{f.GroupId}"); });
                        break;

                    case "5":
                        var studentCount = studentRepository.GetAllStudentCount(@"select count(Journal.InDate) as InDateCount, Students.GroupId from Journal, Students
                            join Groups on Groups.Id=GroupId
                            group by GroupId
							order by InDateCount desc");
                        //studentCount.OrderBy(x => x.InDate.Date);
                        students = studentRepository.GetAllStudent(@"select Students.Id, Students.FirstName, Students.LastName, Students.GroupId, Groups.GroupName from Students 
                            join  Groups on Groups.Id=GroupId").ToList();
                        var resultCount = students.Where(x => x.GroupId == studentCount.FirstOrDefault().GroupId).ToList().Distinct();

                        foreach (var item in resultCount)
                        {
                            Console.WriteLine(item.GroupName);
                        }

                        break;

                    case "6":
                        students = studentRepository.GetAllStudent(@"select * from Students 
                            join Groups on Groups.Id=GroupId
                            join Journal on Journal.StudentId=Students.Id").ToList();
                        students.OrderBy(x => x.InDate.Date);
                        students.ForEach(f => { Console.WriteLine($"Student Id: {f.Id}\t{f.FirstName}\t{f.LastName}\t{f.GroupName}\tGroup Id:{f.GroupId}\t{f.InDate}\t{f.OutDate}"); });
                        break;

                    default:
                        break;
                }

            } while (Console.ReadKey().Key != ConsoleKey.Escape);
        }
    }
}
