﻿using ado.net_lesson12.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace ado.net_lesson12.Repository
{
   public class StudentRepository
    {
        private string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Source\ado.net\ado.net_lesson12\ado.net_lesson12.Repository\Database.mdf;Integrated Security=True";
        public T Insert<T>(string query, T myObject)
        {
            using (var sql = new SqlConnection(connectionString))
            {
                var result = sql.Execute(query, myObject);
                if (result < 1) throw new Exception("Error while Insert");
            }
            Console.WriteLine("Insert Done Successfully");
            return myObject;
        }
        public T Update<T>(string query, T myObject)
        {
            using (var sql = new SqlConnection(connectionString))
            {
                var result = sql.Execute(query, myObject);
                if (result < 1) throw new Exception("Error while Update");
            }
            Console.WriteLine("Update Done Successfully");
            return myObject;
        }

        public T Delete<T>(string query, T myObject)
        {
            using (var sql = new SqlConnection(connectionString))
            {
                var result = sql.Execute(query, myObject);
                if (result < 1) throw new Exception("Error while Delete");
            }
            Console.WriteLine(" Done Successfully");
            return myObject;
        }
        public List<StudentGroupJoinResult> GetAllStudent(string query)
        {
            using (var sql = new SqlConnection(connectionString)) 
            {
                return sql.Query<StudentGroupJoinResult>(query).ToList();
            }
        }

        public List<StudentGroupJoinResult> GetAllStudentCount(string query)
        {
            using (var sql = new SqlConnection(connectionString))
            {
                return sql.Query<StudentGroupJoinResult>(query).ToList();
            }
        }


    }
}
