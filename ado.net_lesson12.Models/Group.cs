﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_lesson12.Models
{
    public class Group
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
    }
}
