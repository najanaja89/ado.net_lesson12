﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ado.net_lesson12.Models
{
    public class StudentGroupJoinResult
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string GroupName { get; set; }
        public int GroupId { get; set; }
        public DateTime InDate { get; set; }
        public DateTime OutDate { get; set; }
        public int InDateCount { get; set; }
    }
}